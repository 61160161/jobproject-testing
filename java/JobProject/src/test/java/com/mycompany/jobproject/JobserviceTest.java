/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.jobproject;

import java.time.LocalDate;
import java.time.Month;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author User
 */
public class JobserviceTest {
    
    public JobserviceTest() {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
    }
    
    @AfterEach
    public void tearDown() {
    }

    /**
     * Test of checkEnableTime method, of class JobService.
     */
    @Test
    public void testCheckEnableTimeTodayBetweenStartAndEnd() {
        System.out.println("checkEnableTime");
        //ARRANGE
        LocalDate stratTime = LocalDate.of(2021, 2, 13);
        LocalDate endTime = LocalDate.of(2021, 2, 27);
        LocalDate today = LocalDate.of(2021, 2, 17);
        boolean expResult = true;
        // ACT
        boolean result = Jobservice.checkEnableTime(stratTime , endTime , today);
        //ASSERT
        assertEquals(expResult, result);
    }
    @Test
    public void testCheckEnableTimeTodayBeforeStart() {
        System.out.println("checkEnableTime");
        //ARRANGE
        LocalDate stratTime = LocalDate.of(2021, 2, 13);
        LocalDate endTime = LocalDate.of(2021, 2, 27);
        LocalDate today = LocalDate.of(2021, 2, 10);
        boolean expResult = false;
        // ACT
        boolean result = Jobservice.checkEnableTime(stratTime , endTime , today);
        //ASSERT
        assertEquals(expResult, result);
    }
    @Test
    public void testCheckEnableTimeTodayAfterEnd() {
        System.out.println("checkEnableTime");
        //ARRANGE
        LocalDate stratTime = LocalDate.of(2021, 2, 13);
        LocalDate endTime = LocalDate.of(2021, 2, 27);
        LocalDate today = LocalDate.of(2021, 2, 28);
        boolean expResult = false;
        // ACT
     boolean result = Jobservice.checkEnableTime(stratTime , endTime , today);
        //ASSERT
        assertEquals(expResult, result);
    }
    @Test
    public void testCheckEnableTimeTodayEqualStart() {
        System.out.println("checkEnableTime");
        //ARRANGE
        LocalDate stratTime = LocalDate.of(2021, 2, 13);
        LocalDate endTime = LocalDate.of(2021, 2, 27);
        LocalDate today = LocalDate.of(2021, 2, 13);
        boolean expResult = true;
        // ACT
       boolean result = Jobservice.checkEnableTime(stratTime , endTime , today);
        //ASSERT
        assertEquals(expResult, result);
    }
    @Test
    public void testCheckEnableTimeTodayEqualEnd() {
        System.out.println("checkEnableTime");
        //ARRANGE
        LocalDate stratTime = LocalDate.of(2021, 2, 13);
        LocalDate endTime = LocalDate.of(2021, 2, 27);
        LocalDate today = LocalDate.of(2021, 2, 27);
        boolean expResult = true;
        // ACT
        boolean result = Jobservice.checkEnableTime(stratTime , endTime , today);
        //ASSERT
        assertEquals(expResult, result);
    }

    
}

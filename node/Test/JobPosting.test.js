const { describe } = require('mocha');
const assert = require("assert");
const { checkEnableTime } = require("../jobPosting");

describe("JobPosting", function () {
  //Arr
  it("shoud return false when เวลาสมัครอยู่ก่อนเริ่มต้น", function () {
    const startTime = new Date(2021, 2, 13);
    const endTime = new Date(2021, 2, 27);
    const today = new Date(2021, 2, 3);
    const expectedResult = false;
    //Act
    const actualResult = checkEnableTime(startTime, endTime, today);
    //Assert
    assert.strictEqual(actualResult, expectedResult);
  });

  it("shoud return false when เวลาสมัครอยู่ายในเวลา", function () {
    const startTime = new Date(2021, 2, 13);
    const endTime = new Date(2021, 2, 27);
    const today = new Date(2021, 2, 17);
    const expectedResult = true;
    //Act
    const actualResult = checkEnableTime(startTime, endTime, today);
    //Assert
    assert.strictEqual(actualResult, expectedResult);
  });

  it("shoud return false when เวลาสมัครอยู่หลังเวลาสุด", function () {
    const startTime = new Date(2021, 2, 13);
    const endTime = new Date(2021, 2, 27);
    const today = new Date(2021, 2, 28);
    const expectedResult = false;
    //Act
    const actualResult = checkEnableTime(startTime, endTime, today);
    //Assert
    assert.strictEqual(actualResult, expectedResult);
  });

  it("shoud return false when เวลาสมัครตรงกับเวลาเริ่ม", function () {
    const startTime = new Date(2021, 2, 13);
    const endTime = new Date(2021, 2, 27);
    const today = new Date(2021, 2, 13);
    const expectedResult = true;
    //Act
    const actualResult = checkEnableTime(startTime, endTime, today);
    //Assert
    assert.strictEqual(actualResult, expectedResult);
  });
  
  it("shoud return false when เวลาสมัครตรงกับเวลาสิ้นสุด", function () {
    const startTime = new Date(2021, 2, 13);
    const endTime = new Date(2021, 2, 27);
    const today = new Date(2021, 2, 27);
    const expectedResult = true;
    //Act
    const actualResult = checkEnableTime(startTime, endTime, today);
    //Assert
    assert.strictEqual(actualResult, expectedResult);
  });
  
});
